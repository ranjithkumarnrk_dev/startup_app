import 'package:flutter/material.dart';

class Constants {
  static const Color PRIMARY_COLOR = Colors.blueAccent;
  static const Color SECONDARY_COLOR = Color(0xff7C4DFF);
  static const Brightness DARK = Brightness.dark;
  static const Brightness LIGHT = Brightness.light;
  static const TEXT_BODY1 = TextStyle(
    fontSize: 24,
    fontStyle: FontStyle.italic
  );
  // ignore: non_constant_identifier_names
  static Color PRODUCT_SLIDER_COLOR = Colors.white;
}