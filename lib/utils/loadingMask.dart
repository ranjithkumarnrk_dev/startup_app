import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';

class LoadingMask extends StatelessWidget {
  final bool _showLoader;
  LoadingMask(this._showLoader);
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _showLoader,
      child: Container(
        color: Colors.white54,
        child: SpinKitChasingDots(
          color: Colors.blue,
          size: 50.0,
        ),
      )
    );
  }
}
