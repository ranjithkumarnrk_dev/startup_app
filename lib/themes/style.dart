import 'package:flutter/material.dart';

const MapRouteColor = Colors.black;

const LargeTextSize = 26.0;
const MediumTextSize = 20.0;
const BodyTextSize = 16.0;

const PrimaryColor = Color(0xff1b1b1b);
const AccentColor = Color(0xffe0e0e0);
const Dark = Brightness.dark;
const Light = Brightness.light;

const String FontNameDefault = 'Montserrat';

const AppBarTextStyle = TextStyle(
    fontFamily: FontNameDefault,
    fontWeight: FontWeight.w300,
    fontSize: MediumTextSize,
    color: Colors.white
);

const TitleTextStyle = TextStyle(
    fontFamily: FontNameDefault,
    fontWeight: FontWeight.w300,
    fontSize: LargeTextSize,
    color: Colors.black
);

const BodyTextStyle = TextStyle(
    fontFamily: FontNameDefault,
    fontWeight: FontWeight.w300,
    fontSize: BodyTextSize,
    color: Colors.black
);


