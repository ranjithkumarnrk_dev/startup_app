import 'package:flutter/material.dart';
import 'package:startup/components/views/search/searchResults.dart';
import 'package:startup/components/common/viewWrapper.dart';
import 'package:startup/components/views/maps/mapsResults.dart';
import 'package:startup/components/views/listings/listings.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (_) => ViewWrapper(
                  child: Home(),showSearch: false,
                ));
      case '/maps':
        return MaterialPageRoute(
            builder: (_) => ViewWrapper(
                  child: MapsResults(),
                ));
      case '/listings':
        return MaterialPageRoute(
            builder: (_) => ViewWrapper(
                  child: Listings(),
                ));
      case '/maps-lists':
        return MaterialPageRoute(
            builder: (_) => ViewWrapper(
                  child: SearchResults(),
                ));
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Homepage Title', style: Theme.of(context).textTheme.headline6),
          Text('Homepage Body', style: Theme.of(context).textTheme.bodyText2),
          const SizedBox(height: 30),
          RaisedButton(
            onPressed: () {
              Navigator.of(context).pushNamed("/maps-lists");
            },
            child: const Text('Search bikes', style: TextStyle(fontSize: 20)),
          ),
        ],
      ),
    );
  }
}
