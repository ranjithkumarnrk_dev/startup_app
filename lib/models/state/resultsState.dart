import 'package:flutter/foundation.dart';
import 'package:startup/domain/item.dart';
import 'package:startup/domain/response.dart';

class ResultsState with ChangeNotifier {
  Map _currentProduct = Map();
  Map _currentRoute = Map();
  List<Item> _productResults = List();
  bool _showProductSlider = false;

  Map get currentProduct => _currentProduct;
  Map get currentRoute => _currentRoute;

  void setCurrentProduct(product) {
    _currentProduct = product;
    notifyListeners();
  }

  void setCurrentRoute(route) {
    _currentRoute = route;
    notifyListeners();
  }

  List<Item> get productResults => _productResults;

  void addProductsToList(List<Item> value) {
    _productResults.addAll(value);
    notifyListeners();
  }

  bool get showProductSlider => _showProductSlider;

  void setShowProductSlider(bool value) {
    _showProductSlider = value;
    notifyListeners();
  }
}
