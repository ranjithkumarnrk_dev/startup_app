import 'package:flutter/foundation.dart';

class MainState with ChangeNotifier {
  bool _shouldShowLoader = true;

  bool get shouldShowLoader => _shouldShowLoader;

  void showLoader(bool shouldLoad) {
    _shouldShowLoader = shouldLoad;
    notifyListeners();
  }
}
