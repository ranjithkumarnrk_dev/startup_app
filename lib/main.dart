import 'package:flutter/material.dart';
import 'products/productsView.dart';
import 'utils/constants.dart';
import 'drawer/menu.dart';
import 'utils/loadingMask.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _showMask = true;
  ThemeData _buildTheme() {
    return ThemeData(
        brightness: Constants.LIGHT,
        primaryColor: Constants.PRIMARY_COLOR,
        accentColor: Constants.SECONDARY_COLOR,
        textTheme: TextTheme(body1: Constants.TEXT_BODY1));
  }

  void loadingMask(shouldMask) {
    setState(() {
      _showMask = shouldMask;
    });
  }
  Widget _buildHome() {
    return Scaffold(
      appBar: AppBar(title: Text('Explore')),
      drawer: Menu(),
      body: Stack(
        children: <Widget>[
          ProductsView(loadingMask),
          LoadingMask(_showMask),
        ],
      ),
    );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Start up',
      theme: _buildTheme(),
      home: _buildHome(),
    );
  }
}
