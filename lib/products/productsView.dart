
import 'package:flutter/material.dart';
import 'maps/mapView.dart';

class ProductsView extends StatefulWidget {
  final Function _loadingMask;
  ProductsView(this._loadingMask) : super();
  @override
  _ProductsViewState createState() => _ProductsViewState(_loadingMask);
}

class _ProductsViewState extends State<ProductsView> {
  final Function _loadingMask;
  _ProductsViewState(this._loadingMask);
  @override
  Widget build(BuildContext context) {
    return MapView(_loadingMask);
  }
}

