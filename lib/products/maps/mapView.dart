import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'productSlide.dart';
import 'mapsRoute.dart';
import 'mapsServices.dart';

class MapView extends StatefulWidget {
  final Function _loadingMask;
  MapView(this._loadingMask) : super();
  @override
  MapViewState createState() => MapViewState(_loadingMask);
}

class MapViewState extends State<MapView> {
  GoogleMapController _mapController;
  Position _currentPosition = Position(latitude: 41.1177, longitude: -73.4082);
  List<Object> _products = List();
  bool _showSlider = false;
  Map _currentProduct = Map();
  Set<Polyline> _polyLines = Set();
  String _distance;

  final Function _loadingMask;
  MapViewState(this._loadingMask) {
    _setCurrentLocation();
    _loadProducts();
  }

  Marker _getMarker(ind, product) {
    double iconColor = BitmapDescriptor.hueCyan;
    if (product['providerName'] == 'VOGO')
      iconColor = BitmapDescriptor.hueViolet;
    if (product['providerName'] == 'NEW') iconColor = BitmapDescriptor.hueGreen;

    return Marker(
        markerId: MarkerId('provider$ind'),
        onTap: () => _onMarkerTap(product),
        position: LatLng(
            product['location']['latLng'][0], product['location']['latLng'][1]),
        infoWindow: InfoWindow(
            title: product['providerName'], snippet: product['price']),
        icon: BitmapDescriptor.defaultMarkerWithHue(iconColor));
  }

  void _onMarkerTap(product) {
    _loadingMask(false);
    _drawRoute(LatLng(
        product['location']['latLng'][0], product['location']['latLng'][1]));
    setState(() {
      _showSlider = true;
      _currentProduct = product;
      _moveMap(product['location']['latLng'][0], product['location']['latLng'][1]);
    });
  }

  void _drawRoute(destination) async {
    LatLng origin =
        LatLng(_currentPosition.latitude, _currentPosition.longitude);
    MapsServices mapsServices = MapsServices(origin, destination);
    MapsRoute mapsRoute = new MapsRoute(origin);
    Map directions = await mapsServices.getDirection();
    String route = directions["routes"][0]["overview_polyline"]["points"];
    mapsRoute.createRoute(route);
    print(directions);
    String distance = directions["legs"][0]["distance"]["text"];
    setState(() {
      _polyLines = mapsRoute.polyLines;
      _distance = distance;
      _loadingMask(false);
    });
  }

  void _loadProducts() {
    loadProductList().then((productsString) {
      Map<String, dynamic> data = jsonDecode(productsString);
      List products = data['products'];
      setState(() {
        _products = products;
      });
    }).catchError((error) {
      print(error);
    });
  }

  List<Marker> _createMarkers() {
    List<Marker> markers = List();
    _products.asMap().forEach((ind, product) {
      markers.add(_getMarker(ind, product));
    });
    return markers;
  }

  void _moveMap(double lat, double long) {
    _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 11,
      tilt: 0.0,
      bearing: 45.0,
    )));
  }

  Future<String> loadProductList() async {
    return await rootBundle.loadString('lib/utils/productList.json');
  }

  void _setCurrentLocation() async {
    if (await Permission.location.request().isGranted) {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      setState(() {
        _currentPosition = position;
      });
    }
    _loadingMask(false);
  }

  Widget _buildMap() {
    return GoogleMap(
      onTap: (LatLng latLng) {
        setState(() {
          _showSlider = false;
        });
      },
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
        zoom: 10,
      ),
      onMapCreated: (controller) {
        _mapController = controller;
      },
      polylines: _polyLines,
      zoomGesturesEnabled: true,
      scrollGesturesEnabled: true,
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: _createMarkers().toSet(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [_buildMap(), ProductSlide(_showSlider, _currentProduct, _distance)],
      ),
    );
  }
}
