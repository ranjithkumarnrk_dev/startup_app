import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';

final String apiKey = 'AIzaSyBqvkdqVcvPfSNfTE43kHKC90c20EImpgA';

class MapsRoute {
  LatLng _currentLocation;
  final Set<Polyline> _polyLines = Set();
  Set<Polyline> get polyLines => _polyLines;

  MapsRoute(this._currentLocation);

  void createRoute(String encodedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId(_currentLocation.toString()),
        width: 6,
        points: _convertToLatLng(_decodePoly(encodedPoly)), color: Colors.blueAccent));
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      }
      while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);
    for (var i = 2; i < lList.length; i++)
      lList[i] += lList[i - 2];
    print(lList.toString());
    return lList;
  }
}
