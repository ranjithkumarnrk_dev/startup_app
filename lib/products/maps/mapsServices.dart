import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

final String apiKey = 'AIzaSyBqvkdqVcvPfSNfTE43kHKC90c20EImpgA';

class MapsServices {
  LatLng _origin;
  LatLng _destination;
  MapsServices(this._origin, this._destination);

  Future<Map> getDirection() async {
    String url =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${_origin.latitude},${_origin.longitude}&destination=${_destination.latitude},${_destination.longitude}&key=$apiKey";
    http.Response response = await http.get(url);
    Map values = JsonDecoder().convert(response.body);
    return values;
  }
}
