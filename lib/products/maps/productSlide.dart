import 'package:flutter/material.dart';
import '../../utils/constants.dart';

class ProductSlide extends StatelessWidget {
  final bool _showSlider;
  final Map _currentProduct;
  final String _distance;

  ProductSlide(this._showSlider, this._currentProduct, this._distance);

  Widget _buildSliderBox() {
    String providerName = _currentProduct["providerName"];
    return _showSlider
        ? Container(
            child: Text(_distance),
          )
        : SizedBox.shrink();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return Align(
      alignment: Alignment.bottomCenter,
      child: AnimatedContainer(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Constants.PRODUCT_SLIDER_COLOR,
        ),
        alignment: Alignment.center,
        duration: Duration(milliseconds: 400),
        height: _showSlider ? _height / 6 : 0,
        width: _width,
        child: _buildSliderBox(),
      ),
    );
  }
}
