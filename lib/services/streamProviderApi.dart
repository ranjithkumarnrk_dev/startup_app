import 'dart:async';
import 'dart:math';
import 'package:startup/domain/response.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class StreamProviderApi {
  final Duration _streamInterval = Duration(seconds: 5);
  StreamController<Response> streamController = StreamController<Response>();
  static int _counter = 2;

  void _loadProduct() async {
    _counter--;
    String _resUrl = "lib/utils/response.json";
    if (_counter == 1) {
      _resUrl = "lib/utils/response2.json";
    }
    String productString = await rootBundle.loadString(_resUrl);
    Map jsonData = jsonDecode(productString);
    Response response = Response.fromJson(jsonData);
    streamController.add(response);
  }

  StreamController<Response> streamProviderList() {
    Timer.periodic(_streamInterval, (timer) {
      if (!streamController.isClosed && _counter > 0) _loadProduct();
    });
    return streamController;
  }

  void dispose() {
    streamController.close();
  }
}
