class Location {
  List<double> _latLng;
  Address _address;

  Location({List<double> latLng, Address address}) {
    this._latLng = latLng;
    this._address = address;
  }

  List<double> get latLng => _latLng;
  set latLng(List<double> latLng) => _latLng = latLng;
  Address get address => _address;
  set address(Address address) => _address = address;

  Location.fromJson(Map<String, dynamic> json) {
    _latLng = json['latLng'].cast<double>();
    _address =
        json['address'] != null ? new Address.fromJson(json['address']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latLng'] = this._latLng;
    if (this._address != null) {
      data['address'] = this._address.toJson();
    }
    return data;
  }
}

class Address {
  String _address1;
  String _address2;

  Address({String address1, String address2}) {
    this._address1 = address1;
    this._address2 = address2;
  }

  String get address1 => _address1;
  set address1(String address1) => _address1 = address1;
  String get address2 => _address2;
  set address2(String address2) => _address2 = address2;

  Address.fromJson(Map<String, dynamic> json) {
    _address1 = json['address1'];
    _address2 = json['address2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address1'] = this._address1;
    data['address2'] = this._address2;
    return data;
  }
}
