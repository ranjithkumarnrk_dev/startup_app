import 'package:flutter/material.dart';

class ListMapToggle extends StatelessWidget {
  final bool _isMapView;
  final Function _onToggleChange;
  ListMapToggle(this._isMapView,this._onToggleChange);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
            ),
            child: ToggleButtons(
              fillColor: Theme.of(context).primaryColor,
              constraints: BoxConstraints(
                minHeight: 30,
                minWidth: 40,
              ),
              selectedColor: Theme.of(context).accentColor,
              color: Theme.of(context).primaryColor,
              children: <Widget>[
                Icon(Icons.map),
                Icon(Icons.list),
              ],
              isSelected: [_isMapView, !_isMapView],
              onPressed: _onToggleChange,
            ),
          ),
        ),
      ),
    );
  }
}
