import 'package:flutter/material.dart';
import 'package:startup/domain/item.dart';

class ProductTile extends StatelessWidget {
  final Item _product;
  ProductTile(this._product);

  Widget _sponsoredBadge(isSponsored) {
    return isSponsored
        ? Align(
            alignment: Alignment.topRight,
            child: Container(
              margin: EdgeInsets.only(right: 14, top: 8),
              padding: EdgeInsets.all(6.0),
              decoration: BoxDecoration(
                color: Colors.deepPurple,
                //borderRadius: BorderRadius.all(Radius.circular(16.0)),
              ),
              child: Text(
                "Sponsored",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
              ),
            ),
          )
        : SizedBox.shrink();
  }

  @override
  Widget build(BuildContext context) {
    //Map address = _product.provider[0].//_product["location"]["address"];
    //bool sponsored = _product.containsKey("sponsored");
    double price = _product.provider[0].prices[0].charge.totalPrice;
    String feature1 = _product.itemFeatures[0].value;
    String feature2 = _product.itemFeatures[1].value;
    String image = _product.provider[0].resources[0].images[0].source;
    return Container(
      margin: EdgeInsets.only(right: 8.0, left: 8.0, top: 5.0, bottom: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        border: Border.all(color: Colors.white),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(3, 6), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Image.network(image),
            height: 100.00,
            width: 100.00,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                feature1,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 6.0,
              ),
              Text(
                feature2,
                style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.blueAccent,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
            ],
          )),
          Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    price.toString(),
                    style: TextStyle(
                        fontSize: 30.0,
                        color: Colors.greenAccent,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
