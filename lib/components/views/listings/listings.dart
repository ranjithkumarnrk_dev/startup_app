import 'package:flutter/material.dart';
import 'package:startup/domain/item.dart';
import 'package:startup/models/state/resultsState.dart';
import 'package:startup/components/views/listings//productTile.dart';
import 'package:provider/provider.dart';

class Listings extends StatefulWidget {
  @override
  _ListingsState createState() => _ListingsState();
}

class _ListingsState extends State<Listings> {
  ResultsState _resultsState;
  Widget _buildListings() {
    List<Item> _products = _resultsState.productResults;
    List<Widget> slides = List();
    _products.asMap().forEach((ind, product) {
      Widget slide = ProductTile(product);
      slides.add(slide);
    });
    return ListView(
      children: slides,
    );
  }

  @override
  Widget build(BuildContext context) {
    _resultsState = Provider.of<ResultsState>(context);
    return Container(
      margin: EdgeInsets.only(top: 75.0),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: _buildListings(),
    );
  }
}
