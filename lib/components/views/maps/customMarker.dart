import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CustomMarker {
  static Future<BitmapDescriptor> createMarkerImage(String provider) async {
    String iconPath = 'assets/bounce.png';
    if (provider == "VOGO") {
      iconPath = 'assets/vogo.png';
    }
    ImageConfiguration configuration = ImageConfiguration(
      size: Size(30, 30),
    );
    BitmapDescriptor bitmapImage =
        await BitmapDescriptor.fromAssetImage(configuration, iconPath);
    return bitmapImage;
  }
}
