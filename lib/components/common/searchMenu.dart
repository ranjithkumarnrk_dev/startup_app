import 'package:flutter/material.dart';
import 'package:startup/utils/constants.dart';

class SearchMenu extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey;
  final bool _showMenu;
  final bool _showSearch;
  SearchMenu(this._scaffoldKey,this._showMenu,this._showSearch);
  static BuildContext _context;

  Widget _searchField() {
    return Container(
        margin: const EdgeInsets.only(left: 10.0, right: 8.0, top: 10.00),
        height: 40,
        //duration: Duration(milliseconds: 600),
        decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(_context).primaryColor,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextFormField(
          inputFormatters: [

          ],
          decoration: InputDecoration(
              icon: _searchIcon(),
              border: InputBorder.none,
              hintText: 'Search',
              hintStyle: TextStyle(
                fontSize: 16,
                color: Colors.grey,
              )),
        ));
  }

  Widget _searchIcon() {
    return Container(
      width: 26.00,
      height: 50.00,
      child: IconButton(
          icon: Icon(
            Icons.search,
            size: 20,
            color: Theme.of(_context).primaryColor,
          ),
          onPressed: () {
            //_scaffoldKey.currentState.openDrawer();
          }),
    );
  }

  Widget _menuIcon() {
    return IconButton(
        icon: Icon(
          Icons.menu,
          size: 40,
          color: Theme.of(_context).primaryColor,
        ),
        onPressed: () {
          _scaffoldKey.currentState.openDrawer();
        });
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Container(
      child: Row(
        children: [_showMenu ? _menuIcon(): SizedBox.shrink(), _showSearch ? Expanded(child: _searchField()): SizedBox.shrink()],
      ),
    );
  }
}
