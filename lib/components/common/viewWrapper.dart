import 'package:flutter/material.dart';
import 'package:startup/components/common/menu.dart';
import 'searchMenu.dart';

class ViewWrapper extends StatelessWidget {
  ViewWrapper({
    Key key,
    @required this.child,
    this.showSearch = true,
    this.showMenu = true,
  }) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final Widget child;
  final bool showSearch;
  final bool showMenu;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(title: Text('Explore')),
      key: _scaffoldKey,
      drawer: Menu(),
      body: Stack(
        children: <Widget>[
          child,
          SafeArea(child: SearchMenu(_scaffoldKey, showMenu, showSearch))
        ],
      ),
    );
  }
}
